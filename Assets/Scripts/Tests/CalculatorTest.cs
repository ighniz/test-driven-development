using Calculator;
using NUnit.Framework;

namespace Tests
{
    public class CalculatorTest
    {
        private ICalculator calculator;
        
        [SetUp]
        public void InitializeTest()
        {
            calculator = new CalculatorV1();
        }
        
        [TestCase(0, 0, 0)]
        [TestCase(0, 1, 1)]
        [TestCase(1, 0, 1)]
        [TestCase(0, -1, -1)]
        [TestCase(5, 5, 10)]
        [TestCase(5, -5, 0)]
        public void AddTest_FloatNumbers_Summation(float a, float b, float expectedResult)
        {
            float result = calculator.Add(a, b);
            Assert.That(result, Is.EqualTo(expectedResult), $"La suma de {a} + {b} debería dar como resultado {expectedResult}.");
        }

        [TestCase(0, 0, 0)]
        [TestCase(0, 1, -11)]
        [TestCase(1, 0, 1)]
        [TestCase(0, -1, 1)]
        [TestCase(5, 5, 0)]
        [TestCase(5, -5, 10)]
        public void SubtractTest_FloatNumbers_Summation(float a, float b, float expectedResult)
        {
            float result = calculator.Subtract(a, b);
            Assert.That(result, Is.EqualTo(expectedResult), $"La resta de {a} - {b} debería dar como resultado {expectedResult}.");
        }
        
        [TestCase(0, 0, float.PositiveInfinity)]
        [TestCase(0, 1, 0)]
        [TestCase(1, 0, float.PositiveInfinity)]
        [TestCase(0, -1, 0)]
        [TestCase(5, 5, 1)]
        [TestCase(5, -5, -1)]
        public void DivideTest_FloatNumbers_Summation(float a, float b, float expectedResult)
        {
            float result = calculator.Divide(a, b);
            Assert.That(result, Is.EqualTo(expectedResult), $"La división de {a} / {b} debería dar como resultado {expectedResult}.");
        }
        
        [TestCase(0, 0, 0)]
        [TestCase(0, 1, 0)]
        [TestCase(1, 0, 0)]
        [TestCase(0, -1, 0)]
        [TestCase(5, 5, 25)]
        [TestCase(5, -5, -25)]
        public void MultiplyTest_FloatNumbers_Summation(float a, float b, float expectedResult)
        {
            float result = calculator.Multiply(a, b);
            Assert.That(result, Is.EqualTo(expectedResult), $"La multiplicación de {a} * {b} debería dar como resultado {expectedResult}.");
        }
    }
}